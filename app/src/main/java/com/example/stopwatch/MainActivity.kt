package com.example.stopwatch

import android.os.AsyncTask
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private var stopwatchThread: AsyncTask<Unit, Unit, Unit>? = null
    private var toggleStartReset: Boolean = false
    private var togglePauseContinue: Boolean = false
    private var numSeconds = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        reset()


        toggleStartResetBtn.setOnClickListener {
            toggleStartReset = !toggleStartReset
            if(toggleStartReset){
                toggleStartResetBtn.text = "Start"
                stopwatchThread?.cancel(true)
                reset()
            }else{
                toggleStartResetBtn.text = "Reset"
                stopwatchThread = DoInBackground()
                stopwatchThread?.execute()
            }


        }

        togglePauseContinueBtn.setOnClickListener {

            if(togglePauseContinue){
                togglePauseContinueBtn.text = "Pause"
                stopwatchThread = DoInBackground()
                stopwatchThread?.execute()
            }else{
                togglePauseContinueBtn.text = "Continue"
                stopwatchThread?.cancel(true)
            }
            togglePauseContinue = !togglePauseContinue

        }
    }

    fun timeFormat(sec: Int) {
        val hours = sec / 360
        val minutes = (sec % 360) / 60
        val seconds = (sec % 360) % 60

        val h = if (hours < 10) "0$hours" else "$hours"
        val m = if (minutes < 10) "0$minutes" else "$minutes"
        val s = if (seconds < 10) "0$seconds" else "$seconds"

        val time = "$h:$m:$s"

        stopWatchCountView.text = time
    }

    override fun onDestroy() {
        stopwatchThread?.cancel(true)
        super.onDestroy()
    }

    private fun reset() {
        togglePauseContinueBtn.isEnabled = false
        numSeconds = 0
        togglePauseContinueBtn.text = "Pause"
        togglePauseContinue = false
        toggleStartReset = true
        timeFormat(numSeconds)
    }

    inner class DoInBackground : AsyncTask<Unit, Unit, Unit>() {

        override fun onPreExecute() {
            super.onPreExecute()
            togglePauseContinueBtn.isEnabled = true
            toggleStartResetBtn.isEnabled = true
        }

        override fun doInBackground(vararg params: Unit?) {
            while (true) {
                Thread.sleep(1000)
                this.publishProgress()
            }
        }

        override fun onProgressUpdate(vararg values: Unit?) {
            super.onProgressUpdate(*values)
            numSeconds++
            timeFormat(numSeconds)
        }

    }

}
